<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Menu extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id', 'name', 'detail',
        'image', 'price'
    ];

    public function getImagePathAttribute()
    {
        return URL::to('/assets/images/menus/'.$this->image);
    }
}
