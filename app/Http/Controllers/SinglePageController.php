<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Menu;
use App\Models\Order;

class SinglePageController extends Controller
{
    public function dashboard()
    {
        $js = 'components.js.dashboard';

        if(myRole() == 'Admin') {
            $data   = [
                'js' => $js
            ];
        } else {
            $hasCurrentOrder = Order::join('carts', 'carts.order_id', '=', 'orders.id')
                ->where('carts.user_id', me()->id)
                ->where('orders.order_status_id', '<', 4)
                ->first();
            
            if($hasCurrentOrder) {
                $currentOrder = Order::join('carts', 'carts.order_id', '=', 'orders.id')
                    ->join('order_statuses', 'order_statuses.id', '=', 'orders.order_status_id')
                    ->where('carts.user_id', me()->id)
                    ->where('orders.order_status_id', '<', 4)
                    ->select([
                        'orders.*', 'order_statuses.name as status'
                    ])
                    ->first();
            } else {
                $currentOrder = NULL;
            }

            $data   = [
                'currentOrder'  => $currentOrder,
                'js'            => $js
            ];
        }

        return view('BE.dashboard', $data);
    }

    public function menu()
    {
        if(me()) {
            $carts  = Cart::join('menus', 'menus.id', '=', 'carts.menu_id')
                ->where('carts.user_id', me()->id)
                ->whereNull('order_id')
                ->select([
                    'menus.name', 'carts.id', 'carts.menu_id'
                ])
                ->get();
        } else {
            $carts = [];
        }

        $categories = Category::all();
            
        $js = 'components.js.index';

        $menus = Menu::get();

        $total = 0;

        if(count($carts) > 0) {
            foreach($carts as $cart) {
                $menu_id = $cart->menu_id;
    
                $price = Menu::where('id', $menu_id)
                    ->first()
                    ->price;
    
                $total = intval($total + $price);
            }
        }

        $data = [
            'carts'         => $carts,
            'categories'    => $categories,
            'js'            => $js,
            'menus'         => $menus,
            'total'         => $total
        ];

        return view('FE.menu', $data);
    }
}
