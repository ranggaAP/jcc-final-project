<?php

namespace App\Http\Controllers;

use App\Models\Voucher;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class VoucherController extends Controller
{

    public function index()
    {
            $js = 'components.js.masterData.vouchers';


            $data   = [
                'js'    => $js,
            ];

            return view('BE.vouchers', $data);
    }

    public function show($id)
    {

        if(is_numeric($id)) {
            $data   = Voucher::where('id', $id)
                ->first();

            return Response::json($data);
        } else {
            // $orders = Order::all();
            // $data = Voucher::join('orders','orders.code','=','vouchers.code')
            // ->select([
            //     'vouchers.*','orders.code as kode'
            // ])
            // ->orderBy('vouchers.code', 'ASC');
            $data = Voucher::orderBy('code','ASC');

            return DataTables::of($data)
            ->editColumn(
                'value',
                function($row) {
                    return 'Rp. '.number_format($row->value);
                }
            )

                ->addColumn(
                    'action',
                    function ($row) {
                        $id = $row->id;

                        $data   = [
                            'id'    => $id,
                        ];

                        return view('components.buttons.masterData.menuActionButtons', $data);

                    }
                )
                ->addIndexColumn()
                ->make(true);
        }
    }

    public function store(Request $request)
    {
        $code = $request->code;
        $value = $request->value;

        if($code == NULL) {
            $json = [
                'msg'       => 'Mohon isi code',
                'status'    => FALSE
            ];
        } else if($value == NULL) {
            $json = [
                'msg'       => 'Mohon isi value',
                'status'    => FALSE
            ];
        } else {

            try {
                $data   = [
                    'code' =>$code,
                    'value' => $value
                ];

                Voucher::create($data);

                $json = [
                    'msg'       => 'Voucher berhasil ditambahkan',
                    'status'    => TRUE
                ];
            } catch(Exception $e) {
                $json   = [
                    'line'      => $e->getLine(),
                    'message'   => $e->getMessage(),
                    'msg'       => 'Error',
                    'status'    => FALSE
                ];
            }
        }

        return Response::json($json);
    }

    public function update(Request $request, $id)
    {
        if(is_numeric($id)) {
            $code = $request->code;
            $value = $request->value;

            if($code == NULL) {
                $json = [
                    'msg'       => 'Mohon isi nama Kode',
                    'status'    => FALSE
                ];
            } else if($value == NULL){
                if($orders == NULL) {
                    $json = [
                        'msg'       => 'Mohon isi nama value',
                        'status'    => FALSE
                    ];
                }
            } else {
                try {
                    $data  = [
                        'code' =>$code,
                        'value' => $value
                    ];

                    Voucher::where('id', $id)
                        ->update($data);

                    $json = [
                        'msg'       => 'Voucher berhasil diperbarui',
                        'status'    => TRUE
                    ];
                } catch(Exception $e) {
                    $json   = [
                        'line'      => $e->getLine(),
                        'message'   => $e->getMessage(),
                        'msg'       => 'Error',
                        'status'    => FALSE
                    ];
                }
            }
        } else {
            $id = $request->id;

            Voucher::where('id', $id)
                ->delete();

            $json = [
                'msg'       => 'Voucher berhasil dihapus',
                'status'    => TRUE
            ];
        }

        return Response::json($json);
    }

    public function match(Request $request)
    {
        $code   = $request->code;
        $total  = $request->total;

        $query = Voucher::where('code', $code)
            ->first();

        if($query) {
            $id     = $query->id;
            $value  = $query->value;

            $total = floatval($total - $value);

            $json   = [
                'discount'  => $value,
                'id'        => $id,
                'status'    => TRUE,
                'total'     => $total
            ];
        } else {
            $json   = [
                'status'    => FALSE
            ];
        }

        return Response::json($json);
    }


}
