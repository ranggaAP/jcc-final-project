<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\Category;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\Facades\DataTables;

class MenuController extends Controller
{
    public function index()
    {
        $js = 'components.js.masterData.menu';
        $kategori = Category::all();

        $data   = [
            'js'    => $js,
            'kategori' => $kategori
        ];

        return view('BE.menu', $data);
    }

    public function show($id)
    {

        if(is_numeric($id)) {
            $data   = Menu::where('id', $id)
                ->first();

            return Response::json($data);
        } else {
            $kategori = Category::all();
            $data = Menu::join('categories','categories.id','=','menus.category_id')
            ->select([
                'menus.*','categories.name as category'
            ])
            ->orderBy('menus.name', 'ASC');

            return DataTables::of($data)
            ->editColumn(
                'price',
                function($row) {
                    return 'Rp. '.number_format($row->price);
                }
            )
            ->editColumn(
                'image',
                function($row) {
                    return '<img src="'.URL::to('/assets/images/menus/'.$row->image).'" width="200" height="200"/>';
                }
            )
                ->addColumn(
                    'action',
                    function ($row) {
                        $id = $row->id;

                        $data   = [
                            'id'    => $id,
                        ];

                        return view('components.buttons.masterData.menuActionButtons', $data);

                    }
                )
                ->addIndexColumn()
                ->rawColumns([
                    'image'
                ])
                ->make(true);
        }
    }

    public function store(Request $request)
    {
        $kategori_id = $request->kategori_id;
        $name = $request->name;
        $detail = $request->detail;
        $file = $request->file('image');
        $price = $request->price;

        if($name == NULL) {
            $json = [
                'msg'       => 'Mohon isi nama menu',
                'status'    => FALSE
            ];
        } else if($detail == NULL) {
            $json = [
                'msg'       => 'Mohon isi detail pada menu',
                'status'    => FALSE
            ];
        } else if($file == NULL) {
            $json = [
                'msg'       => 'Mohon masukan gambar maks 2 MB',
                'status'    => FALSE
            ];
        } else if($price == NULL) {
            $json = [
                'msg'       => 'Mohon isi harga untuk menu',
                'status'    => FALSE
            ];
        } else {
            try {
                $extension = $file->getClientOriginalextension();

                $image = strtotime(date('Y-m-d H:i:s')).'-'.$extension;

                $destination = base_path('public/assets/images/menus');

                $file->move($destination,$image);

                $data   = [
                    'category_id' =>$kategori_id,
                    'name' => $name,
                    'detail' => $detail,
                    'image' => $image,
                    'price' => $price
                ];

                Menu::create($data);

                $json = [
                    'msg'       => 'Menu berhasil ditambahkan',
                    'status'    => TRUE
                ];
            } catch(Exception $e) {
                $json   = [
                    'line'      => $e->getLine(),
                    'message'   => $e->getMessage(),
                    'msg'       => 'Error',
                    'status'    => FALSE
                ];
            }
        }

        return Response::json($json);
    }

    public function update(Request $request, $id)
    {
        if(is_numeric($id)) {
            $kategori_id = $request->kategori_id;
            $name = $request->name;
            $detail = $request->detail;
            $file = $request->file('image');
            $price = $request->price;


            if($name == NULL) {
                $json = [
                    'msg'       => 'Mohon isi nama menu',
                    'status'    => FALSE
                ];
            } else if ($detail == NULL){
                $json = [
                    'msg'       => 'Mohon isi detail menu',
                    'status'    => FALSE
                ];
            } else if($file == NULL) {
                $json = [
                    'msg'       => 'Mohon isi gambar maks 2 MB',
                    'status'    => FALSE
                ];
            } else if($price == NULL) {
                $json = [
                    'msg'       => 'Mohon isi nama harga untuk menu',
                    'status'    => FALSE
                ];
            }
                try {

                    $extension = $file->getClientOriginalextension();

                    $image = strtotime(date('Y-m-d H:i:s')).'-'.$extension;

                    $destination = base_path('public/assets/images/menus');

                    $file->move($destination,$image);

                    $data   = [
                        'category_id' =>$kategori_id,
                        'name' => $name,
                        'detail' => $detail,
                        'image' => $image,
                        'price' => $price
                    ];

                    Menu::where('id', $id)
                        ->update($data);

                    $json = [
                        'msg'       => 'Menu berhasil diperbarui',
                        'status'    => TRUE
                    ];
                } catch(Exception $e) {
                    $json   = [
                        'line'      => $e->getLine(),
                        'message'   => $e->getMessage(),
                        'msg'       => 'Error',
                        'status'    => FALSE
                    ];
                }
        } else {
            $id = $request->id;

            Menu::where('id', $id)
                ->delete();

            $json = [
                'msg'       => 'Menu berhasil dihapus',
                'status'    => TRUE
            ];
        }

        return Response::json($json);
    }
}
