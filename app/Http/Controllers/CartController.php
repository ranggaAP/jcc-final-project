<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class CartController extends Controller
{
    public function add(Request $request)
    {
        $id         = $request->id;
        $user_id    = me()->id;

        $cartCheck = Cart::where('carts.user_id', $user_id)
            ->where('menu_id', $id)
            ->whereNull('order_id')
            ->count();

        if($cartCheck == 1) {
            $json = [
                'status'    => FALSE,
            ];
        } else {
            $menu = Menu::where('id', $id)
                ->first();
    
            $data = [
                'user_id'   => $user_id,
                'menu_id'   => $id,
            ];
    
            $newCart = Cart::create($data);
    
            $carts = Cart::where('user_id', $user_id)
                ->whereNull('order_id')
                ->get();
    
            $total = 0;
    
            foreach($carts as $cart) {
                $menu_id = $cart->menu_id;
    
                $price = Menu::where('id', $menu_id)
                    ->first()
                    ->price;
    
                $total = intval($total + $price);
            }
    
            $json = [
                'count'     => count($carts),
                'id'        => $newCart->id,
                'name'      => $menu->name,
                'status'    => TRUE,
                'total'     => 'Rp. '.number_format($total)
            ];
        }

        return Response::json($json);
    }
    
    public function checkout()
    {
        $carts  = Cart::join('menus', 'menus.id', '=', 'carts.menu_id')
            ->where('carts.user_id', me()->id)
            ->whereNull('order_id')
            ->select([
                'menus.name', 'carts.id', 'carts.menu_id',
                'menus.price'
            ])
            ->get();

        if(count($carts) == 0) {
            return Redirect::back();
        }

        $js = 'components.js.checkout';

        $total = 0;

        foreach($carts as $cart) {
            $menu_id = $cart->menu_id;

            $price = Menu::where('id', $menu_id)
                ->first()
                ->price;

            $total = intval($total + $price);
        }

        $data = [
            'carts'         => $carts,
            'js'            => $js,
            'total'         => $total
        ];

        return view('FE.checkout', $data);
    }

    public function remove(Request $request)
    {
        $id         = $request->id;
        $user_id    = me()->id;

        Cart::where('id', $id)
            ->delete();

        $carts = Cart::where('user_id', $user_id)
            ->whereNull('order_id')
            ->get();

        $total = 0;

        if(count($carts) > 0) {
            foreach($carts as $cart) {
                $menu_id = $cart->menu_id;
    
                $price = Menu::where('id', $menu_id)
                    ->first()
                    ->price;
    
                $total = intval($total + $price);
            }
        }

        $json = [
            'count' => count($carts),
            'total' => 'Rp. '.number_format($total)
        ];

        return Response::json($json);
    }
}
