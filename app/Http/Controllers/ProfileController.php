<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDetail;
use Exception;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\Facades\DataTables;

class ProfileController extends Controller
{
    public function index()
    {
        $js = 'components.js.masterData.profile';
        $user = User::all();
        $user_detail = UserDetail::all();

        $data   = [
            'js'    => $js,
            'user' => $user,
            'user_detail' => $user_detail
        ];

        return view('BE.profile', $data);
    }

    public function show($id)
    {
        if(is_numeric($id)) {
            $data   = User::where('id', $id)
                ->first();

            return Response::json($data);
        } else {
            $user = User::all();
            $data = UserDetail::join('users','users.id','=','user_details.user_id')
            ->select([
                'user_detail.*','users.name',
            ])
            ->where('id','=','2');

            return DataTables::of($data)
                ->addColumn(
                    'action',
                    function ($row) {
                        $id = $row->id;


                        $data   = [
                            'id'    => $id
                        ];

                        return view('components.buttons.masterData.profileActionButtons', $data);

                    }
                )
                ->addIndexColumn()
                ->make(true);
        }
    }

}
