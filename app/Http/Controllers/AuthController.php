<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\UserDetail;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email'=> 'required|email|unique:users',
            'password'=>'required|confirmed|min:8',
            'address'=>'required'
        ];

        $message = [
            'name.required' => 'Nama wajib diisi',
            'email.required' => 'Email wajib diisi',
            'email.email' => 'Email tidak valid',
            'email.unique' => 'Email sudah terdaftar',
            'password.required' => 'Password wajib diisi',
            'password.min' => 'password minimal 8 karakter',
            'password.confirmed' => 'password tidak sesuai',
            'address.required' => 'Alamat wajib diisi'
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        } else {
            try {
                $name = $request->name;
                $email = $request->email;
                $password = Hash::make($request->password);
                $address = $request->address;

                $data = [
                    'name' => $name,
                    'email' => $email,
                    'password' => $password,
                ];

                $user = User::create($data);

                if ($user) {
                    $user_id = $user->id;

                    $user->syncRoles('User');

                    $data = [
                        'address' => $address,
                        'user_id' => $user_id,
                    ];

                    UserDetail::create($data);

                    return Redirect::to('/login')
                        ->with('success','Registrasi berhasil silakan masuk');
                }
            } catch (Exception $e) {
                $log = [
                    'msg' => $e->getMessage(),
                    'line' => $e->getLine(),
                ];

                dd($log);
            }
        }
    }

    public function registerView()
    {
        return view('auth.register');
    }

    public function loginView()
    {
        return view('auth.login');
    }

    public function login(Request $request) {
        $rules = [
            'email'=> 'required|email',
            'password'=>'required|min:8',
        ];

        $message = [
            'email.required' => 'Email wajib diisi',
            'email.email' => 'Email tidak valid',
            'password.required' => 'Password wajib diisi',
            'password.min' => 'password minimal 8 karakter'
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        } else {
            try {
                $email      = $request->email;
                $password   = $request->password;

                $attempt = [
                    'email'     => $email, 
                    'password'  => $password,
                ];

                if(Auth::attempt($attempt)) {
                    return Redirect::to('/');
                } else {
                    return Redirect::back()
                        ->with('danger', 'Email atau password salah')
                        ->withInput();
                }
                
            } catch (Exception $e) {
                $log = [
                    'msg' => $e->getMessage(),
                    'line' => $e->getLine(),
                ];

                dd($log);
            }
        }
    }

    public function logout()
    {
        Auth::logout();

        return Redirect::to('/');
    }

}
