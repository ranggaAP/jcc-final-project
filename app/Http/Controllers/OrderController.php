<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{
    public function changeStatus($id)
    {
        $previous_status = Order::where('id', $id)
            ->first()
            ->order_status_id;

        $newStatus = ($previous_status + 1);

        $data   = [
            'order_status_id'   => $newStatus
        ];

        Order::where('id', $id)
            ->update($data);

        $data   = [
            'order_id'          => $id,
            'order_status_id'   => $newStatus,
        ];

        OrderLog::create($data);

        $json = [
            'status' => TRUE
        ];

        return Response::json($json);
    }

    public function checkout(Request $request)
    {
        $code           = generateCode();
        $grand_total    = $request->grand_total;
        $discount       = $request->discount;
        $total          = $request->total;
        $user_id        = me()->id;
        $voucher_id     = $request->voucher_id;

        $data = [
            'code'              => $code,
            'grand_total'       => $grand_total,
            'discount'          => $discount,
            'order_status_id'   => 1,
            'total'             => $total,
            'voucher_id'        => $voucher_id,
        ];

        $order = Order::create($data);

        $order_id = $order->id;

        $data   = [
            'order_id'          => $order_id,
            'order_status_id'   => 1,
        ];

        OrderLog::create($data);

        $carts = Cart::where('user_id', $user_id)
            ->whereNull('order_id')
            ->get();
        
        foreach($carts as $cart) {
            $data   = [
                'order_id'  => $order_id
            ];

            Cart::where('id', $cart->id)
                ->update($data);
            
            return Redirect::to('/dashboard')
                ->with('success','Pesanan anda sudah terdaftar pada dapur kami');
        }
    }

    public function show($id)
    {
        if(is_numeric($id)) {
            $carts  = Cart::join('menus', 'menus.id', '=', 'carts.menu_id')
                ->where('carts.order_id', $id)
                ->select([
                    'menus.name', 'carts.id', 'carts.menu_id',
                    'menus.price'
                ])
                ->get();
    
            $logQuery   = OrderLog::join('order_statuses', 'order_statuses.id', '=', 'order_logs.order_status_id')
                ->where('order_logs.order_id', $id)
                ->select([
                    'order_statuses.created_at', 'order_statuses.name as status'
                ])
                ->get();
    
            foreach($logQuery as $log) {
                $logs[]   = [
                    'date'      => tanggalIndoFull($log->created_at),
                    'status'    => $log->status,
                ];
            }
    
            $order = Order::join('order_statuses', 'order_statuses.id', '=', 'orders.order_status_id')
                ->where('orders.id', $id)
                ->select([
                    'orders.*', 'order_statuses.name as status'
                ])
                ->first();
    
            $json = [
                'carts' => $carts,
                'logs'  => $logs,
                'order' => $order
            ];
    
            return Response::json($json);
        } else {
            $data   = Order::join('order_statuses', 'order_statuses.id', '=', 'orders.order_status_id')
                ->where('orders.order_status_id', '<', 4)
                ->select([
                    'orders.*', 'order_statuses.name as status'
                ]);

            return DataTables::of($data)
                ->editColumn(
                    'code',
                    function ($row) {
                        return '#'.$row->code;
                    }
                )
                ->addColumn(
                    'user',
                    function ($row) {
                        $id = $row->id;

                        $user = Cart::join('users', 'users.id', '=', 'carts.user_id')
                            ->where('carts.order_id', $id)
                            ->select([
                                'users.name'
                            ])
                            ->first()
                            ->name;

                        return $user;
                    }
                )
                ->addColumn(
                    'action',
                    function ($row) {
                        $id = $row->id;

                        $data   = [
                            'id'    => $id
                        ];

                        return view('components.buttons.adminDashboardActionButtons', $data);

                    }
                )
                ->addIndexColumn()
                ->make(true);
        }
    }
}
