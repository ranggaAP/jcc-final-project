<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{
    public function index()
    {
        $js = 'components.js.masterData.categories';

        $data   = [
            'js'    => $js
        ];

        return view('BE.categories', $data);
    }

    public function show($id)
    {
        if(is_numeric($id)) {
            $data   = Category::where('id', $id)
                ->first();

            return Response::json($data);
        } else {
            $data = Category::orderBy('name', 'ASC');

            return DataTables::of($data)
                ->addColumn(
                    'action',
                    function ($row) {
                        $id = $row->id;

                        $data   = [
                            'id'    => $id
                        ];

                        return view('components.buttons.masterData.categoryActionButtons', $data);

                    }
                )
                ->addIndexColumn()
                ->make(true);
        }
    }

    public function store(Request $request)
    {
        $name = $request->name;

        if($name == NULL) {
            $json = [
                'msg'       => 'Mohon isi nama kategori menu',
                'status'    => FALSE
            ];
        } else {
            try {
                $data   = [
                    'name' => $name
                ];

                Category::create($data);

                $json = [
                    'msg'       => 'Kategori berhasil ditambahkan',
                    'status'    => TRUE
                ];
            } catch(Exception $e) {
                $json   = [
                    'line'      => $e->getLine(),
                    'message'   => $e->getMessage(),
                    'msg'       => 'Error',
                    'status'    => FALSE
                ];
            }
        }

        return Response::json($json);
    }

    public function update(Request $request, $id)
    {
        if(is_numeric($id)) {
            $name = $request->name;

            if($name == NULL) {
                $json = [
                    'msg'       => 'Mohon isi nama kategori menu',
                    'status'    => FALSE
                ];
            } else {
                try {
                    $data   = [
                        'name' => $name
                    ];

                    Category::where('id', $id)
                        ->update($data);

                    $json = [
                        'msg'       => 'Kategori berhasil diperbarui',
                        'status'    => TRUE
                    ];
                } catch(Exception $e) {
                    $json   = [
                        'line'      => $e->getLine(),
                        'message'   => $e->getMessage(),
                        'msg'       => 'Error',
                        'status'    => FALSE
                    ];
                }
            }
        } else {
            $id = $request->id;

            Category::where('id', $id)
                ->delete();

            $json = [
                'msg'       => 'Kategori berhasil dihapus',
                'status'    => TRUE
            ];
        }

        return Response::json($json);
    }
}
