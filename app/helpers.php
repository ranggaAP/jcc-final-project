<?php

use App\Models\Order;
use Illuminate\Support\Facades\Auth;

function generateCode() {
    $orderNumber = Order::whereDate('created_at', date('Y-m-d'))
        ->count();

    if($orderNumber == 0) {
        return date('dmY').'0001';
    } elseif($orderNumber > 0 && $orderNumber < 10) {
        return date('dmY').'000'.$orderNumber;
    } elseif($orderNumber > 10 && $orderNumber < 100) {
        return date('dmY').'00'.$orderNumber;
    } elseif($orderNumber > 100 && $orderNumber < 1000) {
        return date('dmY').'0'.$orderNumber;
    } else {
        return date('dmY').$orderNumber;
    }
}

function hariIndo($N) {
    $hari   = [
        '1'       => 'Senin', 
        'Selasa', 'Rabu', 'Kamis', 
        'Jumat', 'Sabtu', 'Minggu'
    ];
    
    return $hari[$N];
}

function me() {
    return Auth::user();
}

function myRole() {
    return Auth::user()->getRoleNames()[0];
}

function tanggalIndo($date) {
    $bulan = [
        1 =>   'Januari',
        'Februari', 'Maret','April',
        'Mei', 'Juni', 'Juli',
        'Agustus', 'September', 'Oktober',
        'November', 'Desember'
    ];
    
	$split = explode('-', $date);
    
	return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
}

function tanggalIndoFull($date) {
    return hariIndo(date('N', strtotime($date))).', '.tanggalIndo(date('Y-m-d', strtotime($date))).' '.date('H:i', strtotime($date));
}
