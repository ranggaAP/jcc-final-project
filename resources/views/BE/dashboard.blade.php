@extends('layouts.app')
@section('content')
@push('head')
<link rel="stylesheet" href="{{ asset('/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
<style>
    .scrollableModalDialog {
        overflow-y: initial !important;
    }

    .scrollableModalBody {
        height: 75vh; 
        overflow-y: auto;
    }
</style>
@endpush
<div class="content-wrapper">
    <section class="content">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="alert alert-success" role="alert" style="width: 100%;">
                    <strong>{{ Session::get('success') }}</strong>
                    @php
                    Session::forget('success');
                    @endphp
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @elseif(Session::has('danger'))
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert" style="width: 100%;">
                    <strong>{{ Session::get('danger') }}</strong>
                    @php
                    Session::forget('danger');
                    @endphp
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif
        @role('Admin')
        <div class="card mt-2">
            <div class="card-body">
                <table id="table" class="table table-bordered table-striped w-100">
                    <thead>
                        <th>#</th>
                        <th>Pelanggan</th>
                        <th>Kode</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        @endrole
        @role('User')
        <div class="card mt-2">
            <div class="card-body">
            @if($currentOrder)
                <div class="card">
                    <div class="card-header">
                        <h2>Pesanan Aktif Anda</h2>
                    </div>
                    <div class="card-body">
                        <h3>Pesanan no. #{{ $currentOrder->code }}</h3>
                        <p>Status saat ini : {{ $currentOrder->status }}</p>
                        <a href="javascript:void(0)" class="btn bg-primary my-3" onClick="viewMenu({{ $currentOrder->id }})">
                            Lihat detail pesanan saya
                        </a>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <p>Anda tidak memiliki pesanan aktif saat ini</p>
                        <a href="{{ URL::to('/') }}" class="btn bg-primary my-3">Pesan sekarang!</a>
                    </div>
                </div>
            @endif
            </div>
        </div>
        @endrole
    </section>
</div>
@include('components.modals.cartDetail')
@push('script')
    <script src="{{ asset('/assets/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    @include($js)
@endpush
@endsection