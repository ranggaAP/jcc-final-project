@extends('layouts.app')
@section('content')
@push('head')
    <link rel="stylesheet" href="{{ asset('/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
<div class="content-wrapper">
    <input type="hidden" id="id">
    <section class="content">
        <div class="card mt-2">
            <div class="card-header">
                <h3 class="card-title">Kategori Makanan</h3>
            </div>
            <div class="card-body">
                @include('components.buttons.addDataButton')
                <table id="table" class="table table-bordered table-striped w-100">
                    <thead>
                        <th>#</th>
                        <th>Nama Kategori</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </section>
</div>
@include('components.modals.categories.create')
@include('components.modals.categories.edit')
@push('script')
    <script src="{{ asset('/assets/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    @include($js)
@endpush
@endsection