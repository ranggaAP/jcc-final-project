@extends('layouts.blank')
@section('content')
<form action="{{ URL::to('/checkout') }}" method="POST">
    @csrf
    <input type="hidden" name="discount" id="discount">
    <input type="hidden" name="voucher_id" id="voucher_id">
    <input type="hidden" name="total" value="{{ $total }}">
    <input type="hidden" name="grand_total" id="grand_total" value="{{ $total }}">
    <div class="content-wrapper">
        <section class="content">
            <div class="card mt-2">
                <div class="card-header">
                    <h4>Pesanan Anda</h4>
                </div>
                <div class="card-body">
                    <table class="table table-hover table-striped">
                        <thead>
                            <th>#</th>
                            <th>Menu</th>
                            <th>Harga</th>
                        </thead>
                        <tbody>
                            @foreach($carts as $cart)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $cart->name }}</td>
                                    <td>Rp. {{ number_format($cart->price) }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td>Voucher</td>
                                <td>
                                    <input type="text" id="voucherInput" class="form-control">
                                </td>
                                <td id="printDiscount">
                                    Rp. 0
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-center">Grand Total</td>
                                <td id="printTotal">Rp. {{ number_format($total) }}</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-center"></td>
                                <td>
                                    <button class="btn btn-success">Pesan</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</form>
@push('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    @include($js)
@endpush
@endsection