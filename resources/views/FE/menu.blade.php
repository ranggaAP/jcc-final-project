@extends('layouts.menu')
@section('content')
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

      @if(count($menus) > 0)
        <div class="row mt-2">
          <div class="col-md-4"></div>
          <div class="col-md-4"></div>
          <div class="col-md-4 text-center">
            <div class="form-group">
              <label for="categories">Kategori</label>
              <select id="categories" class="form-control">
                <option value="">Semua</option>
                @foreach($categories as $category)
                  <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          @foreach($menus as $menu)
          <div class="col-md-4 menu text-center" data-id="{{ $menu->id }}" data-category-id="{{ $menu->category_id }}">
            <div class="card">
              <div class="card-body">
                <img src="{{ $menu->imagePath }}" alt="{{ $menu->name }}" width="200" height="200" class="img">
                <h3 class="menuName">{{ $menu->name }}</h3>
                <p>{{ $menu->detail }}</p>
                <p>Rp. {{ number_format($menu->price) }}</p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      @else
      <div class="card">
        <div class="card-body">
          <p>Coming Soon</p>
        </div>
      </div>
      @endif

    </section>
    <!-- /.content -->
  </div>
  @push('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    @include($js)
  @endpush
@endsection