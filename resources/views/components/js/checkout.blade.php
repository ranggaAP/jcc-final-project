<script>
    const number_format = (number, decimals, dec_point, thousands_sep) => {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }

    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        $('#voucherInput').bind('keyup keydown keypress paste', function (e) {
            var total   = "{{ $total }}";
            var code    = $(this).val();

            $.ajax({
                type: "POST",
                url: "/apply-voucher",
                data: {
                    code,
                    total
                },
                dataType: "JSON",
                success: function (response) {
                    if(response.status) {
                        $('#printDiscount').text(`Rp. ${number_format(response.discount,0,'.',',')}`);
                        $('#printTotal').text(`Rp. ${number_format(response.total,0,'.',',')}`);

                        $('#discount').val(response.discount);
                        $('#grand_total').val(response.total);
                        $('#voucher_id').val(response.id);
                    } else {
                        $('#printDiscount').text('Rp. 0');
                        $('#printTotal').text(`Rp. ${number_format(total,0,'.',',')}`);

                        $('#discount').val('');
                        $('#grand_total').val(total);
                        $('#voucher_id').val('');
                    }
                }
            });
        });
    });
</script>