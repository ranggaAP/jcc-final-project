<script>
    @role('Admin')
        const nextStatus = (id) => {
            $.ajax({
                type: "POST",
                url: `/change-status/${id}`,
                dataType: "JSON",
                success: function (response) {
                    $('#table').DataTable().ajax.reload();
                }
            });
        }
    @endrole

    const number_format = (number, decimals, dec_point, thousands_sep) => {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }

    const viewMenu = (id) => {
        $.ajax({
            type: "GET",
            url: `/order-detail/${id}`,
            dataType: "JSON",
            success: function (response) {
                $('#cartMenuModal').modal('show');

                $('#cartMenuCode').text(`Pesanan #${response.order.code}`);

                $('#cartMenuContainer').html('');

                $('#cartMenuContainer').append(`
                    <h4>Pesanan Anda</h4>
                    <table class="table table-hover table-striped">
                        <thead>
                            <th>#</th>
                            <th>Menu</th>
                            <th>Harga</th>
                        </thead>
                        <tbody id="cartMenuTable"></tbody>
                    </table>
                `);

                $.each(response.carts, function(index, data) {
                    $('#cartMenuTable').append(`
                        <tr>
                            <td>${index + 1}.</td>
                            <td>${data.name}</td>
                            <td>Rp. ${number_format(data.price,0,'.',',')}</td>
                        </tr>
                    `);
                });

                $('#cartMenuTable').append(`
                    <tr>
                        <td colspan="2">Diskon</td>
                        <td>Rp. ${number_format(response.order.discount,0,'.',',')}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Grand Total</td>
                        <td>Rp. ${number_format(response.order.grand_total,0,'.',',')}</td>
                    </tr>
                `);

                $('#cartMenuContainer').append(`
                    <h4>Log Status Order</h4>
                    <table class="table table-hover table-striped">
                        <thead>
                            <th>#</th>
                            <th>Status</th>
                            <th>Waktu</th>
                        </thead>
                        <tbody id="cartMenuLog"></tbody>
                    </table>
                `);

                $.each(response.logs, function(index, data) {
                    $('#cartMenuLog').append(`
                        <tr>
                            <td>${index + 1}.</td>
                            <td>${data.status}</td>
                            <td>${data.date}</td>
                        </tr>
                    `);
                });
            }
        });
    }

    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        @role('Admin')
            $('#table').DataTable({
                order: [],
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                filter: true,
                processing: true,
                responsive: true,
                serverSide: true,
                processing: true,
                language: {
                processing: '<i class="ace-icon fa fa-spinner fa-spin orange bigger-500" style="font-size:60px;margin-top:50px;"></i>'
                },
                scroller: {
                    loadingIndicator: false
                },
                pagingType: "full_numbers",
                ajax: {
                    url: '/active-order/table'
                },
                "aaSorting": [],
                "bFilter": false,
                "columns":
                [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'user', name: 'user', orderable: false, searchable: false },
                    { data: 'code', name: 'orders.code' },
                    { data: 'status', name: 'order_statuses.name' },
                    { data: 'action', orderable: false, searchable: false },
                ],
            });
        @endrole
    });
</script>