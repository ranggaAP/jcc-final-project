<script>
    const removeCart = (id) => {
        $.ajax({
            type: "POST",
            url: "remove-cart-item",
            data: {
                id
            },
            dataType: "JSON",
            success: function (response) {
                $(`#cartItem${id}`).remove();

                if(response.count > 0) {
                    if($('#cartTotal').is(':empty')) {
                        $('#cartTotal').append(`
                            <hr>
                            <a href="javascript:void(0)" class="nav-link">
                                <p id="totalString">Total: ${response.total}</p>
                            </a>
                        `);
                    } else {
                        $('#totalString').text(`Total: ${response.total}`);
                    }

                    if($('#checkoutLinkContainer').is(':empty')) {
                        $('#checkoutLinkContainer').append(`
                            <hr>
                            <a href="{{ URL::to('/checkout') }}" class="nav-link">
                                <p>Checkout</p>
                                <i class="nav-icon fas fa-shopping-cart"></i>
                            </a>
                        `);
                    }
                } else {
                    $('#cartTotal').html('');
                    $('#checkoutLinkContainer').html('');
                }
            }
        });
    }
    
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        
        $('#search').bind('keyup keydown keypress paste', function (e) {
            escape = function(text) {
              return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
            };
            
            var menus = $('.menu');
            
            var input = escape($(this).val());

            for(i = 0; i < menus.length; i++) {
                if(input == '') {
                    $('.menu').css('display', 'block');
                    
                    break;
                } else {
                    var text = $(`.menuName:eq(${i})`).text();
                    
                    if(text.search(new RegExp(input, "i")) < 0) {
                        $(`.menu:eq(${i})`).css('display', 'none');
                    } else {
                        $(`.menu:eq(${i})`).css('display', 'block');
                    }
                }
            }
        });

        $('#cancelSearch').click(function (e) { 
            e.preventDefault();
            
            $('.menu').css('display', 'block');
        });

        $('#categories').change(function (e) { 
            e.preventDefault();
            
            var value = $(this).val();

            var menus = $('.menu');

            for(i = 0; i < menus.length; i++) {
                if(value == '') {
                    $('.menu').css('display', 'block');
                    
                    break;
                } else {
                    var id = $(`.menu:eq(${i})`).data('category-id');
                    
                    if(value == id) {
                        $(`.menu:eq(${i})`).css('display', 'block');
                    } else {
                        $(`.menu:eq(${i})`).css('display', 'none');
                    }
                }
            }
        });
        @guest
            $('.menu').click(function (e) { 
                e.preventDefault();
                
                Swal.fire({
                    title: 'Daftar heula atuh',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: 'Oh heeh, punten',
                    cancelButtonText: 'Acan daftar, kumaha tah?'
                }).then((result)=>{
                    if(result.value){
                        window.location = "{{ URL::to('/login') }}";
                    } else if(result.dismiss == 'cancel') {
                        window.location = "{{ URL::to('/register') }}";
                    }
                });
            });
        @endguest
        @role('User')
            $('.menu').click(function (e) { 
                e.preventDefault();
                
                var id = $(this).data('id');

                $.ajax({
                    type: "POST",
                    url: "/add-to-cart",
                    data: {
                        id
                    },
                    dataType: "JSON",
                    success: function (response) {
                        if(response.status) {
                            $('#cartContainer').append(`
                                <li class="nav-item" id="cartItem${response.id}">
                                    <a href="javascript:void(0)" class="nav-link">
                                        <p>${response.name}</p>
                                        <i class="nav-icon fas fa-times" onclick="removeCart(${response.id})"></i>
                                    </a>
                                </li>
                            `);

                            if(response.count > 0) {
                                if($('#cartTotal').is(':empty')) {
                                    $('#cartTotal').append(`
                                        <hr>
                                        <a href="javascript:void(0)" class="nav-link">
                                            <p id="totalString">Total: ${response.total}</p>
                                        </a>
                                    `);
                                } else {
                                    $('#totalString').text(`Total: ${response.total}`);
                                }

                                if($('#checkoutLinkContainer').is(':empty')) {
                                    $('#checkoutLinkContainer').append(`
                                        <hr>
                                        <a href="{{ URL::to('/checkout') }}" class="nav-link">
                                            <p>Checkout</p>
                                            <i class="nav-icon fas fa-shopping-cart"></i>
                                        </a>
                                    `);
                                }
                            } else {
                                $('#cartTotal').html('');
                                $('#checkoutLinkContainer').html('');
                            }

                        }
                    }
                });
            });
        @endrole
    });
</script>
