<a href="javascript:void(0)" class="btn bg-primary" onClick="viewMenu({{ $id }})" data-toggle="tooltip" data-placement="top" title="Detail Pesanan">
    <i class="fas fa-eye"></i>
</a>

<a href="javascript:void(0)" class="btn bg-success" onClick="nextStatus({{ $id }})" data-toggle="tooltip" data-placement="top" title="Next Status">
    <i class="fas fa-paper-plane"></i>
</a>