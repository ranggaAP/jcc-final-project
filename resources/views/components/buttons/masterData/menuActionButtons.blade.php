<a href="javascript:void(0)" class="btn bg-primary" onClick="edit({{ $id }})" data-toggle="tooltip" data-placement="top" title="Edit Menu">
    <i class="fas fa-edit"></i>
</a>

<a href="javascript:void(0)" class="btn bg-danger" onClick="deleteData({{ $id }})" data-toggle="tooltip" data-placement="top" title="Hapus Menu">
    <i class="fas fa-trash"></i>
</a>
