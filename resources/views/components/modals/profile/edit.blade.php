<form id="editForm">
    <div class="modal" tabindex="-1" role="dialog" id="editModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Sunting Menu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        </select>
                        <label for="editForm">Namamu</label>
                        <input type="text" name="name" id="editForm" class="form-control" autocomplete="off">
                        <label>Alamat</label>
                        <input type="text" name="address" id="editForm">
                        <br>
                        <label for="">Harga</label>
                        <input type="text" name="price" class="form-control" id="editForm">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary" id="editSubmit">Sunting</button>
                </div>
            </div>
        </div>
    </div>
</form>
