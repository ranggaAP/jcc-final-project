<form id="createForm">
    <div class="modal" tabindex="-1" role="dialog" id="createModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Menu Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Kategori</label>
                        <select name="kategori_id" class="form-control" id="">
                        @foreach ($kategori as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                        </select>
                        <label for="createName">Nama Menu</label>
                        <input type="text" name="name" id="createName" class="form-control" autocomplete="off">
                        <label for="createName">Detail</label>
                        <textarea name="detail" id="createName" class="form-control" cols="30" rows="10"></textarea>
                        <label>Gambar</label>
                        <input type="file" name="image">
                        <br>
                        <label for="">Harga</label>
                        <input type="text" name="price" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary" id="createSubmit">Tambah</button>
                </div>
            </div>
        </div>
    </div>
</form>
