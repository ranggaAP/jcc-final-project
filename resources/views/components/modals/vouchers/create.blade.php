<form id="createForm">
    <div class="modal" tabindex="-1" role="dialog" id="createModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Voucher Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Code</label>
                        {{-- <select name="orders_code" class="form-control" id="">
                        @foreach ($orders as $item)
                            <option value="{{$item->id}}">{{$item->code}}</option>
                        @endforeach
                        </select> --}}
                        <input type="text" name="code" id="createName" class="form-control">
                        <label for="createName">Value</label>
                        <input type="text" name="value" id="createName" class="form-control" autocomplete="off">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary" id="createSubmit">Tambah</button>
                </div>
            </div>
        </div>
    </div>
</form>
