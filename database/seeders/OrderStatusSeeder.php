<?php

namespace Database\Seeders;

use App\Models\OrderStatus;
use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data   = [
            'name'  => 'Waiting'
        ];

        OrderStatus::create($data);

        $data   = [
            'name'  => 'In Kitchen'
        ];

        OrderStatus::create($data);

        $data   = [
            'name'  => 'On Delivery'
        ];

        OrderStatus::create($data);

        $data   = [
            'name'  => 'Done'
        ];

        OrderStatus::create($data);
    }
}
