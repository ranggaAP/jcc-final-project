<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Menu;
use App\Models\OrderStatus;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\Voucher;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class InitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data   = [
            'name'  => 'Admin'
        ];

        $admin = Role::create($data);

        $data   = [
            'name'  => 'User'
        ];

        $customer = Role::create($data);

        $data   = [
            'name'      => 'Admin',
            'email'     => 'admin@DGDN.com',
            'password'  => Hash::make('12345678')
        ];

        $user = User::create($data);

        $user->syncRoles($admin);

        $data   = [
            'user_id' => $user->id,
            'address' => 'Bandung'
        ];

        UserDetail::create($data);

        $data   = [
            'name'      => 'Customer',
            'email'     => 'customer@DGDN.com',
            'password'  => Hash::make('12345678')
        ];

        $user = User::create($data);

        $user->syncRoles($customer);

        $data   = [
            'user_id' => $user->id,
            'address' => 'Bandung'
        ];

        UserDetail::create($data);

        $data   = [
            'name' => 'Makanan'
        ];

        Category::create($data);

        $data   = [
            'name' => 'Minuman'
        ];

        Category::create($data);

        $data   = [
            'name' => 'Cemilan'
        ];

        Category::create($data);

        $data   = [
            'category_id'   => 1,
            'name'          => 'Hamburger',
            'detail'        => 'Patty dibalut roti goreng',
            'image'         => 'saxasxasxwqzx.jpg',
            'price'         => 30000,
        ];

        Menu::create($data);

        $data   = [
            'category_id'   => 2,
            'name'          => 'Coca Cola',
            'detail'        => 'Kola manis dan segar dan baik untuk tenggorokan karena sudah dioplos dengan OBH',
            'image'         => 'rrefecsdd.jpg',
            'price'         => 10000,
        ];
        
        Menu::create($data);

        $data   = [
            'category_id'   => 3,
            'name'          => 'French Fries',
            'detail'        => 'Kentang goreng kering dan gurih',
            'image'         => 'gbtygbrtgvtr.jpg',
            'price'         => 15000,
        ];

        Menu::create($data);

        $data   = [
            'code'  => 'TESTING',
            'value' => 10000
        ];

        Voucher::create($data);

        $data   = [
            'name' => 'Dalam Antrian'
        ];

        OrderStatus::create($data);

        $data   = [
            'name' => 'Diproses di Dapur'
        ];

        OrderStatus::create($data);

        $data   = [
            'name' => 'Dalam Pengantaran'
        ];

        OrderStatus::create($data);

        $data   = [
            'name' => 'Selesai'
        ];

        OrderStatus::create($data);
    }
}
