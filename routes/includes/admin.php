<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\VoucherController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth'], function(){
    Route::group(['middleware' => 'role:Admin'], function(){
        Route::get('/categories',[CategoryController::class,'index']);
        Route::get('/category/{id}',[CategoryController::class,'show']);
        Route::post('/categories',[CategoryController::class,'store']);
        Route::post('/category/{id}',[CategoryController::class,'update']);

        Route::get('/menu',[MenuController::class,'index']);
        Route::get('/menu/{id}',[MenuController::class,'show']);
        Route::post('/menu',[MenuController::class,'store']);
        Route::post('/menu/{id}',[MenuController::class,'update']);

        Route::get('/vouchers',[VoucherController::class,'index']);
        Route::get('/vouchers/{id}',[VoucherController::class,'show']);
        Route::post('/vouchers',[VoucherController::class,'store']);
        Route::post('/vouchers/{id}',[VoucherController::class,'update']);
    });
});


