<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\SinglePageController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'guest'], function(){
    Route::get('/register', [AuthController::class,'registerView']);
    Route::post('/register', [AuthController::class,'register']);
    Route::get('/login', [AuthController::class,'loginView'])->name('login');
    Route::post('/login',[AuthController::class,'login']);
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/dashboard',[SinglePageController::class,'dashboard']);
    Route::post('/logout',[AuthController::class,'logout']);
});


