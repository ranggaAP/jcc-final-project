<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\VoucherController;
use Illuminate\Support\Facades\Route;

Route::post('/search',[CartController::class,'search']);

Route::group(['middleware' => 'auth'], function(){
    Route::group(['middleware' => 'role:User'], function(){
        Route::post('/add-to-cart',[CartController::class,'add']);
        Route::post('/remove-cart-item',[CartController::class,'remove']);

        Route::get('/checkout',[CartController::class,'checkout']);
        Route::post('/checkout',[OrderController::class,'checkout']);
        Route::post('/apply-voucher',[VoucherController::class,'match']);

        Route::get('/order-detail/{id}',[OrderController::class,'show']);

        Route::get('/profile',[ProfileController::class,'index']);
        Route::get('/profile/{id}',[ProfileController::class,'show']);
        Route::post('/profile',[ProfileController::class,'store']);
        Route::post('/profile/{id}',[ProfileController::class,'update']);


    });

    Route::get('/order-detail/{id}',[OrderController::class,'show']);
});
